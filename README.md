chess_tournament
================

Racket package for hosting chess tournaments. Players can access their
games by using unique urls. Each game has three links. One for playing
with the white pieces, one for playing with the black pieces and one
for observing.

# Installation

The program is written in [racket](https://racket-lang.org/), and it
is necessary to have it installed in your computer. It can be
downloaded [here](https://download.racket-lang.org/).

Once racket is installed you can install **chess\_tournament** by
running the following command

```
raco pkg install https://gitlab.com/oquijano/chess_tournament.git
```

The program consists of a command called **chess\_tournament**, in
order to run the command you need to add the racket adds-on folder to
your PATH. You can see what this folder is by running the following
command

```racket
racket -e "(path->string (build-path (find-system-path 'addon-dir) (version) \"bin\"))"
```

Once the command is in your path, you can see the help from the command by running
```racket
chess_tournament -h
```

# Included Components & Licensing

chess_tournament uses the following javascript libraries

| Library       | License                                                                      |
|---------------|------------------------------------------------------------------------------|
| chessboard.js | [MIT License](https://github.com/oakmac/chessboardjs/blob/master/LICENSE.md) |
| chess.js      | [BSD 2](https://github.com/jhlywa/chess.js/blob/master/LICENSE)              |


# Examples of use

After following the instructions from the Installation section you
will have the command `chess_tournament` in your terminal.

## Copying website files to a folder

Create an empty folder an then enter that folder using your
terminal. Then run the following
```sh
chess_tournament copy-files
```

This will copy 6 .html documents, a yaml file called config.yml and a
folder called htdocs to your current folder. The .html are templates
parsed by racket to show the tournament website and send emails to the
players of the tournament. You can edit them or add/modify the css to
personalize them in any way you want. Jus't be careful with the parts
containing @'s, this is racket code that generates html. If you want
to personalize that too, you need fo familiarize with the
[@syntax](https://docs.racket-lang.org/scribble/reader.html) of
[scribble](https://docs.racket-lang.org/scribble/index.html). Here is
a description of each file

+ register.html : Website for registering players in the tournament
+ main-page.html : Main website of the tournament. It contains links for observing games and a table with the points of each player
+ game.html : Page for playing and observing a game
+ email_template.html : Contents of the email sent to players about their next game
+ email_bye_template.html : If there is an odd number of players one of them get's a bye (does not play and get a point). This is the template of the email to send to such player.
+ email_registration_template.html : Template of the confirmation request email to send when a player registers.
+ htdocs : Folder containing three folders
  + img : Contains the images for the chess pieces
  + js : Folder with javascript code for the tournament, chess.js and chessboard.js
  + css : Folder with css files for all the html templates.
  
You can modify any of these files to your own needs, just be careful
with the pats containing @'s, if you want to change that part you
might want to familiarize yourself with the
[@syntax](https://docs.racket-lang.org/scribble/reader.html) of
[scribble](https://docs.racket-lang.org/scribble/index.html).

## Defining games without a tournament

We assume here you are still inside the folder created in the previous
section. In order to have some games without defining a tournament you
need to create a csv file. Each line of the csv file defines a game,
and it should contain the following columns: white name, black name,
white code, black code, observer code, time (optional, in seconds).

For testing purposes, you can create a file called `games.csv` with
the following contents:

```
White player 1,Black player 1,white_code1,black_code1,obs_code1
White player 2,Black player 2,white_code2,black_code2,obs_code2,300
White player 3,Black player 3,white_code3,black_code3,obs_code3,600
```

In the first line there is no time limit for the game, in the second
line there is a 5 minutes time limit (300 seconds) and in the third
one there is a 10 minutes limit (600 seconds).

Make sure you do not have any additional spaces or characters since
they will be considered part of the field. 

After creating the file you can serve these games by running

```sh
chess_tournament -p 8080 -f . start-games games.csv
```

The `-p` option sets the port on which the games are served, the `-f`
option is a path to a folder containing the templates to use. After
running the command above, the links for the game in the first line
are the following:

+ For the white player: http://localhost:8080/game/white_code1
+ For the black player: http://localhost:8080/game/black_code1
+ For the observer: http://localhost:8080/game/obs_code1

From a different computer in the same network you can change localhost
for the ip of the computer where you are serving the games.

## Organizing a tournament

Organizing a tournament requires that you modify the config.yml file
in the folder where you copied the files. The default file looks like
this

```
email:
  from: email@localhost 
  server: localhost
  port: 25
  user:
  password:
  use-ssl?: false # either true or false.
  use-tls?: false # either true or false.
# At most one of use-ssl? or use-tls? should be true.
  
db-file: chess.db

website:
  port: 55000
  url: http://localhost:55000
  
tournament:
  rounds: 3
  time: 300  # in seconds. 0 means no time
  system: swiss # swiss or random
  bbpPairings-path: # only necessary for the swiss system
```

The program sends two types of emails, one of them is for confirming
registration and the other one is at the beginning of each round to
provide the link for playing a game. In the email section you should
write the information about the email account from which emails will
be sent.

*db-file* is the name of a sqlite database where tournament
information will be saved, you can choose any name you would like.

The website section has two elements, *port* which is the port on which
the program will be running and *url* which is the url which will be
used for the tournament. This is the url that will be used in the
emails sent to the players to connect to their game, so, if you are
using a reverse proxy you should put the "external" url here.

The tournament section has 4 fields, *rounds* which is the number of
rounds in the tournament, *time* which is the time available for each
player in a match, *system* either **random** or **swiss**, which
stand for random games and the [swiss
system](https://en.wikipedia.org/wiki/Swiss-system_tournament). For
the swiss system it is necessary to also set the field
*bbpPairings-path* which should be the full path to the program
[bbpPairings](https://github.com/BieremaBoyzProgramming/bbpPairings). To
download it go to the [tags section of the project on
github](https://github.com/BieremaBoyzProgramming/bbpPairings/tags)
and go to the download section of the most recent tag. There you might
find a compiled version for your platform for you to download,
otherwise you will have to compile it yourself.

### Serving the registration website

After setting the config.yml file and possibly personalizing the
website files, you can invite people to register to the tournament by
visiting the url that you put in the *url* field of the website
section and after running the command

```
chess_tournament -f . start-registration
```

If you are in a different folder than where the tournament files are,
you should change '.' above for the path to that folder.

### Starting a tournament

After players have registered, you should stop the program running for
registration, and then run the following program that starts the
tournament

```
chess_tournament -f . start-tournament
```
