function OneButtonBanner(text,button_text="OK",show=true){
    var alert_template = document.querySelector("#alert-template");
    var banner = alert_template.content.querySelector("#alert").cloneNode(true);
    var block  = alert_template.content.querySelector("#block").cloneNode();
    this.message = banner.querySelector(".banner-text");
    let this_aux=this;
    this.on = false;
    this.set_text = function(text){
	this.message.innerHTML = text;
    }
    
    this.set_text(text);

    this.show = function(){
	this.added_block = document.body.appendChild(block);
	this.added_banner = document.body.appendChild(banner);
	this.on=true;
    }
    this.button = banner.querySelector(".banner-button");
    this.button.innerHTML = button_text;
    this.stop = function(){
	document.body.removeChild(this.added_block);
	document.body.removeChild(this.added_banner);
	this.on=false;
    }

    this.button.onclick=function(e){
	this_aux.stop();
    }

    if(show){
	this.show();
    }
}

function CountBanner(){
    var count_banner_template = document.querySelector("#count-banner");
    var banner = count_banner_template.content.querySelector("#banner").cloneNode(true);
    var block  = count_banner_template.content.querySelector("#block").cloneNode();
    this.set_text = function(text){
	banner.innerHTML = text;
    }

    var added_block = document.body.appendChild(block);
    var added_banner = document.body.appendChild(banner);

    this.stop=function(){
	document.body.removeChild(added_block);
	document.body.removeChild(added_banner);
    }
}

function Banner(){
    //Creates a banner object
    var banner_template = document.querySelector("#banner-template");
    var banner = banner_template.content.querySelector("#alert").cloneNode(true);
    var block  = banner_template.content.querySelector("#block").cloneNode();
    this.content = banner.querySelector(".banner-text");
    
    var added_block = document.body.appendChild(block);
    var added_banner = document.body.appendChild(banner);

    this.stop=function(){
	document.body.removeChild(added_block);
	document.body.removeChild(added_banner);
    }    
}

function TwoButtonsBanner(text,button1_text,button2_text,show=true){
    
    var banner_template = document.querySelector("#two-buttons-banner");
    var banner = banner_template.content.querySelector("#banner").cloneNode(true);
    var block  = banner_template.content.querySelector("#block").cloneNode();
    this.message = banner.querySelector(".banner-text");
    this.button1 = banner.querySelector("#button1");
    this.button2 = banner.querySelector("#button2");
    this.button1.innerHTML = button1_text;
    this.button2.innerHTML = button2_text;
    this.message.innerHTML = text;

    this.show=function(){
	document.body.appendChild(block);
	document.body.appendChild(banner);
	this.on=true;
    }
    
    this.stop=function(){
	document.body.removeChild(block);
	document.body.removeChild(banner);
	this.on=false;
    }

    if(show){
	this.show()
    }
    
}

