"use strict";
 
var lala;
var promoting=false;
var websocket_url=window.location.toString().replace("http","ws").replace(/\/[^/]+$/,"");
var sdate_ws_url=window.location.toString().replace("http","ws").replace(/(\/[^/]+){2}$/,"") + "/date";
var page_id = window.location.pathname.substring().substr(window.location.pathname.substring().lastIndexOf("/")+1);
var start_ws_url=window.location.toString().replace("http","ws").replace(/(\/[^/]+){2}$/,"") + `/start_banner/${page_id}`;
 
var clocks_ws_url=window.location.toString().replace("http","ws").replace(/(\/[^/]+){2}$/,"") + `/clocks/${page_id}`;
 
var hostname = window.location.host;
var protocol = window.location.protocol;
 
var start_banner;
 
var clocks = null;
 
var automatic_draw_refuse=false;
 
 
var player_name, player_color, opponent_name, time;
var player_countdown = null;
var opponent_countdown = null;
var observer = false;
var board,
    game = new Chess(),
    statusEl = $('#status'),
    pgnEl = $('#pgn');

var pgn=document.querySelector("#pgn");
 
var buttons_section=document.getElementById("user_buttons");
var resign_button=document.getElementById("resign");
var draw_button=document.getElementById("draw");
var burger=document.getElementById("closed-menu");
 
// The following is to avoid getting many confirm windows when a draw
// is asked.
var draw_request_pending=false;
 
// The following variable is used for tracking whether if someone has
// resigned or a draw has been agreed.
// If it has value "w" it means white won
var user_result="";
 
 
var last_message_index=0;

var lichess_url=null;


 
function get_piece_image_url(a_piece){
    //a_piece should be one of: q,n,r,b,k
    return `/img/chesspieces/wikipedia/${player_color.substring(0,1)}${a_piece.toUpperCase()}.png`
}
 
function erase_buttons(){
    if(buttons_section != null){
	buttons_section.innerHTML="";
    }
    burger.remove()
}

function go_to_lichess(){
    window.location=lichess_url;
}

function add_lichess_reference(reference){
    if(reference != null){
	lichess_url=`https://lichess.org${reference}`;
	buttons_section.innerHTML=`<button onclick="go_to_lichess()">Analyse in lichess</button>`;
    }
}
 
function erase_clock_borders(){
    document.querySelector("#player-clock").style.border="none";
    document.querySelector("#opponent-clock").style.border="none";
}
 
function promotion_menu(f){
    var promotion_banner = new Banner();
    var pieces =  {};
    var pieces_vector=['q','n','r','b'];
    promotion_banner.content.innerHTML=`<a id="q" href=""><img src="${get_piece_image_url('q')}" style="width: 20%;" ></img></a>
<a id="n" href=""><img src="${get_piece_image_url('n')}" style="width: 20%;"></img></a>
<a id="r" href=""><img src="${get_piece_image_url('r')}" style="width: 20%;"></img></a>
<a id="b" href=""><img src="${get_piece_image_url('b')}" style="width: 20%;"></img></a>
`
    for (var i in pieces_vector){
              let piece=pieces_vector[i];
              let selector_string = `#${piece}`;
   pieces[piece]=promotion_banner.content.querySelector(selector_string);
              pieces[piece].onclick = function(e){
                  e.preventDefault();
                  promotion_banner.stop();
		  f(piece);
              }
    }
}
 
                                                              
// do not pick up pieces if the game is over
// only pick up pieces for the side to move
var onDragStart = function(source, piece, position, orientation){
       
    if (observer)
              return false;
 
    if  (game.game_over() === true ||
              (game.turn() === 'w' && piece.search(/^b/) !== -1) ||
              (game.turn() == 'w' && player_color == "black" ) ||
              (game.turn() === 'b' && piece.search(/^w/) !== -1) ||
              (game.turn() === 'b' && player_color == "white")  ||
              (user_result != "")){
              return false;
    }
};
 
function is_game_over(){
    return game.game_over() === true || user_result != '';
}
 
 
var onDrop = function(source, target) {
    // see if the move is legal
    promoting=false;
 
    var move_cfg = {
        from: source,
        to: target,
        promotion: 'q'
    };
   
    var move = game.move(move_cfg);
 
    if (move === null){
              return 'snapback';
    }else{
              game.undo();
    }
 
    var source_rank = source.substring(2,1);
    var target_rank = target.substring(2,1);
    var piece = game.get(source).type;
 
    if (piece === 'p' &&
        ((source_rank === '7' && target_rank === '8') || (source_rank === '2' && target_rank === '1'))){
              promoting=true;
              promotion_menu((x)=>{game.move({from: source,to: target,promotion: x})
                                                board.position(game.fen());
                                                if(clocks != null ){
                                                          clocks.move();
                                                }
                                                updateStatus();
                                                send_update_request();
                                                promoting=false;
                                               });
    }else{
              game.move(move_cfg);
    }
   
 
    if(! promoting){
              updateStatus();
              send_update_request();
    }
   
    if(clocks != null && ! promoting){        
              clocks.move();
    }
 
};
 
 
 
// update the board position after the piece snap
// for castling, en passant, pawn promotion
var onSnapEnd = function() {
    if(! promoting){
              board.position(game.fen());
    }
};
 
var updateStatus = function() {
    var status = '';
 
    var moveColor = 'White';
    if (game.turn() === 'b') {
              moveColor = 'Black';
    }
 
    if(game.game_over() === true || user_result != ''){
              erase_buttons();
    }
 
    // checkmate?
    if (game.in_checkmate() === true) {
              status = 'Game over, ' + moveColor + ' is in checkmate.';
              if(clocks!=null){clocks.stop();}
    }
 
   
 
    // draw?
    else if (game.in_draw() === true) {
              status = 'Game over, drawn position';
              user_result = "d";
              if(clocks!=null){clocks.stop();}
    }

    else if (game.insufficient_material() === true) {
              status = 'Game over, draw due to insufficient material';
              user_result = "d";
              if(clocks!=null){clocks.stop();}
    }

    
 
    else if ( user_result === "w"  ){
              status = "Game over, Black resigned";
              if(clocks!=null){clocks.stop();}
    }
 
    else if ( user_result === "bt"  ){
              status = "Game over, White ran out of time";
              if(clocks!=null){clocks.stop();}
    }
 
 
    else if ( user_result === "b"  ){
              status = "Game over, White resigned";
              if(clocks!=null){clocks.stop();}
    }
 
    else if ( user_result === "wt"  ){
              status = "Game over, Black ran out of time";       
              if(clocks!=null){clocks.stop();}
    }
 
 
    else if(user_result === "d"){
              status = "Game over, players agreed to draw";
              if(clocks!=null){clocks.stop();}
    }
 
    // game still on
    else {
              status = moveColor + ' to move';
 
              // check?
              if (game.in_check() === true) {
                  status += ', ' + moveColor + ' is in check';
              }
    }
 
    statusEl.html(status);
    // pgnEl.html(game.pgn({max_width:5, newline_char: '<br />'  }));
    pgnEl.html(game.pgn());
    pgn.scroll(pgn.scrollLeftMax,0);
    // pgn.scrollTop=pgn.scrollHeight;
};
 
 
var set_up_game = function(a_player_name,a_player_color,an_opponent_name){
   
    var cfg = {
              draggable: true,
              position: 'start',
              onDragStart: onDragStart,
              onDrop: onDrop,
              onSnapEnd: onSnapEnd,
              orientation: a_player_color
    };
 
    board = ChessBoard('board', cfg);
    var page_opponent = document.getElementById('opponent_name');
    var page_player_name = document.getElementById('player_name');
    page_player_name.innerText = a_player_name;
    page_opponent.innerText = an_opponent_name;
    jQuery('#board').on('scroll touchmove touchend touchstart contextmenu', function(e){
	e.preventDefault();
    });
    // set_dimentions();
    board.resize();

};
 
var page_info_dict= {action: "page-info", id: page_id};

function connect_board_websocket(){
    
    var this_aux=this;
    this.reconnecting=false;
    this.connect_websocket=function(){
	window.socket = new WebSocket(websocket_url);

	window.socket.onopen = function(e) {


	    socket.send(JSON.stringify(page_info_dict));
	    
	    send_update_request(true);
	    updateStatus();
	
	    //this_aux.keep_alive_interval=setInterval(()=>{window.socket.send(JSON.stringify(null));},1000*60*2);
	    var interval=setInterval( () =>{
		if( ( window.socket.readyState == window.socket.CLOSING || window.socket.readyState == window.socket.CLOSED ) && ! is_game_over() ){
		    clearInterval(interval);
		    window.socket=null;
		    this_aux.connect_websocket();
		}
	    },5000);

	    window.socket.addEventListener('message',
					   function(event){
					       var message = JSON.parse(event.data);
					       if(message == null){
						   send_update_request();
					       }
					       else{                                  
						   var action = message.action;                                                 
						   switch(action){
						   case "page-info":
						       if( ! this_aux.reconnecting ){
							   player_color = message.player_color;
							   if(player_color == "observer"){
							       observer = true;
							       player_color = "white";
							       erase_buttons();                                                                     
							   }                                                       
							   player_name = message.player_name;
							   opponent_name = message.opponent_name;

							   set_up_game(player_name,player_color,opponent_name);
							   time = message.time;
							   if( time > 0){
							       clocks = new Clocks(time,sdate,clocks_ws_url);
							       if(observer){
								   start_banner = new StartBanner(start_ws_url,sdate,false);
							       }else{
								   start_banner = new StartBanner(start_ws_url,sdate);
							       }
							   }else{							       
							       erase_clock_borders();
							   }
							   this_aux.reconnecting=true;
						       }
						       break;
						   case "update":
						       if(message.pgn != undefined){
							   game.load_pgn(message.pgn);
							   board.position(game.fen());
						       }
						       user_result = message.result;
						       updateStatus();
						       if(is_game_over() && start_banner!= undefined && start_banner.banner.on  ){
							   start_banner.banner.stop();
						       }
						       break;
						   case "ask_draw":
						       if (draw_request_pending){
							   return;
						       }else{
							   draw_request_pending=true;
							   if(automatic_draw_refuse){
							       socket.send(JSON.stringify({action : "refuse_draw"}));
							       //                                                                        draw_request_pending=false;
							   }else{
							       
							       var banner = new TwoButtonsBanner(`Your opponent asks you for draw. Do you accept?<br/><br/><input type="checkbox" id="checkbox" ><label for="checkbox" style="color: #aaaaaa; font-size: smaller;" >Automatically refuse future draw requests.</label></input>`,"No","Yes");
							       
							       var cb = banner.message.querySelector("#checkbox");
							       banner.button1.onclick = function(e){
								   socket.send(JSON.stringify({action : "refuse_draw"}));
								   if(cb.checked){
								       automatic_draw_refuse=true;
								   }
								   banner.stop();
							       }
							       banner.button2.onclick = function(e){
								   socket.send(JSON.stringify({action : "accept_draw", fen : game.fen(), pgn : game.pgn(), status: statusEl.text(), result: user_result}));
								   user_result="d";                                                                          
								   updateStatus();
								   banner.stop();
							       }
							   }   
							   
						       }
						       break;
						   case "draw_response_received":
						       draw_request_pending=false;
						       break;
						   case "draw_accepted":
						       user_result="d";
						       updateStatus();
						       OneButtonBanner("Your opponent accepted the draw request");
						       break;
						   case "draw_refused":
						       OneButtonBanner("Your opponent refused the draw request");
						       draw_button.disabled=false;
						       break;

						   case "lichess":
						       add_lichess_reference(message.reference);
						       break;
						   }}});


	}
    }
    
    this.connect_websocket();
}


new connect_board_websocket();
 
 
function send_update_request(just_opened=false){
    if((! just_opened) &&  clocks != null &&  ( clocks.socket.readyState != clocks.socket.OPEN || socket.readyState != socket.OPEN ) && ! is_game_over() ){
                  window.location.reload();
    }else{
    socket.send(JSON.stringify({action : "update",
                                                          fen : game.fen(),
                                                          pgn : game.pgn(),
                                                          status: statusEl.text(),
                                                          result: user_result,
                                                          draw_request_pending : draw_request_pending
                                                  }));
    }
}                                          
 

 
// resign_button.onclick=function(){
//     if (window.confirm("Do you want to resign?")){
//         if(player_color === "white" && (! game.game_over() ))
//             user_result = "b";
//         else
//             user_result = "w";
//         updateStatus();
//         socket.send(JSON.stringify({action : "resign", fen : game.fen(), pgn : game.pgn(), status: statusEl.text(), result: user_result}));	
//     }
// };
 
 
resign_button.onclick = () =>{
    document.querySelector("#closed-menu").click();
    var banner = new TwoButtonsBanner("Do you want to resign?","No","Yes");
    banner.button1.onclick = function(e){
              banner.stop();
    }
    banner.button2.onclick = function(e){
              if(player_color === "white" && (! game.game_over() ))
                  user_result = "b";
              else
                  user_result = "w";
              updateStatus();
              socket.send(JSON.stringify({action : "resign", fen : game.fen(), pgn : game.pgn(), status: statusEl.text(), result: user_result}));
              banner.stop();
    }
       
}
 
draw_button.onclick = ()=>{
    document.querySelector("#closed-menu").click();
    var banner = new TwoButtonsBanner("Do you want to ask for draw?","No","Yes");
    banner.button1.onclick = function(e){
              banner.stop();
    }
    banner.button2.onclick = function(e){
              socket.send(JSON.stringify({action : "ask_draw"}));
              draw_button.disabled=true;
              banner.stop();
    }
   
}
 
 
updateStatus();
 
var sdate=new serverDate(sdate_ws_url,3600*1000);
 

 
function StartBanner(ws_banner_url,sdate_object,show=true){
    this.sdate=sdate_object;
    var request_text="The game hasn't started yet. Press the button below to request your opponent to start.";
    this.banner = new OneButtonBanner(request_text,"Request Game Start",show);
 
    this.waiting_response=false;
    this.waiting_timeout = null;

    this.stoped=false;
    
    var this_aux = this;

    this.tbanner = undefined;
    this.tbanner = new TwoButtonsBanner("Your opponent is requesting to start the game. Do you accept?","Yes","No",false);
    this.tout=undefined;

    this.tbanner.button1.onclick = function(e){
        this_aux.send_event("accept");
        clearTimeout(this_aux.tout);
        this_aux.tbanner.stop();
    }
    
    this.tbanner.button2.onclick = function(e){
        this_aux.send_event("reject");
        clearTimeout(this_aux.tout);
        this_aux.tbanner.stop();
        this_aux.banner.show();
    }

    


    this.connect_websocket=function(){
	
	this_aux.ws_start = new WebSocket(ws_banner_url);

	this_aux.ws_start.onopen = function(e){
	    //Continua aquí, termina esta función

	    var interval=setInterval( () =>{
		if(   ( this_aux.ws_start.readyState == this_aux.ws_start.CLOSING || this_aux.ws_start.readyState == this_aux.ws_start.CLOSED ) ){
		    clearInterval(interval);
		    this_aux.socket=null;
		    this_aux.connect_websocket();
		}
	    },5000);
	    
	    this_aux.ws_start.onmessage = function(e){
		var message = JSON.parse(e.data);
		this_aux.process_message(message);
	    }
	    
	    this_aux.stop_banner=function(){
		this_aux.banner.stop();
		this_aux.stoped=false;
		clearInterval(interval);
		this_aux.ws_start.close();
	    }
	    
	}
    }

    this.connect_websocket();
   
    this.send_event = function(message){
              var to_send = JSON.stringify({event: message , date: this.sdate.get()})
              this.ws_start.send(to_send);
    }


 
    this.banner.button.onclick = function(e){        
        this_aux.send_event("request_start");
        this_aux.banner.button.style.display="none";
        this_aux.banner.set_text("Waiting for the opponent respond...");
        this_aux.waiting_response = true;
        this_aux.waiting_timeout=setTimeout(()=>{this_aux.request_timeout();
                                             this_aux.waiting_response = false;},11000);
    }
 
    this.make_local_request = function(){

	this_aux.tbanner.show();
	
        if(this.banner.on){
            this.banner.stop();
        }

	
	
        this.tout = setTimeout(()=>{
	    if(this_aux.tbanner.on){
		this_aux.tbanner.stop();
	    }
            this_aux.send_event("timeout");
            this_aux.banner.show();},
                               10000);
	
             
    }
 
    this.process_message = function(message){
              if(message.status != undefined){
                  if(message.status >0 && this_aux.banner.on ){
                             this_aux.banner.stop();
                  }
              }else{       
                  if( message.event == "request_start" ){
                             this.make_local_request();
                  }else if( message.event == "reject"){
                             if(this.waiting_response){
                                 clearTimeout(this.waiting_timeout);
                             }
                             this.waiting_response=false;
                             this.banner.set_text(`Your opponent refused to start the game.<br/>Press the button below to make the request again`);
                             this.banner.show();
                             this.banner.button.style.display="";
                  }else if (message.event == "accept"){
                             if(this.banner.on){
                                 this.banner.stop();
                             }
                             this.start_game_countdown(5,message.date);
                  }
              }
    }
 
    this.start_game_countdown=function(Nseconds,start_time){
              var Nmiliseconds = Nseconds * 1000;
              var cdn_banner = new CountBanner();
              var start_countdown= new Countdown(Nmiliseconds,"banner",false,sdate,1000*3600,false);
              start_countdown.onzero=()=>{
                  cdn_banner.stop();
               
                  if(player_color == "white"){
                             clocks.player_countdown.start(start_time + Nmiliseconds);
                  }else{
                             clocks.opponent_countdown.start(start_time + Nmiliseconds);
                  }
              }
              clocks.init_read=true;
              start_countdown.start(start_time);       
    }
 

 
    this.request_timeout = function(){
              this.banner.set_text(`Your opponent did not answer.<br/>Press the button below to make the request again`);
              this_aux.banner.button.style.display="";            
    }
 
   
}
 
 
function Clocks(time,sdate,ws_clock_url){
    // time should be in seconds
    this.player_countdown = new Countdown(time*1000,"player-clock",false,sdate);
    this.opponent_countdown = new Countdown(time*1000,"opponent-clock",false,sdate);
    this.sdate=sdate;
    this.init_read = false;
    var this_aux=this;

    this.connect_websocket=function(){
	this.socket = new WebSocket(ws_clock_url);

	this.socket.onopen = function(e){


            var interval=setInterval(()=>{
		if(   ( this_aux.socket.readyState == this_aux.socket.CLOSING || this_aux.socket.readyState == this_aux.socket.CLOSED ) && ! is_game_over() ){
		    clearInterval(interval);
		    this_aux.socket=null;
		    this_aux.init_read=false;
		    this_aux.stop();
		    this_aux.connect_websocket();
		}
		if(is_game_over()){
                    this_aux.stop();
		}
            },5000)}


	this.send_event=function(event_type, info=null, date=null){
            if(date  == null){
                let message_to_send={event: event_type , date:this.sdate.get() , info: info};
                this.socket.send( JSON.stringify(message_to_send) );
            }else{
                let message_to_send={event: event_type , date:date, info: info};
                this.socket.send( JSON.stringify(message_to_send));
            }
            
	}
	
	this.move = function(){
            var current_date = sdate.get();
            this.player_countdown.stop();
	    this.send_event("move",this.player_countdown.available_time,current_date);
            this.opponent_countdown.start(current_date);
	}
	
	this.player_countdown.onzero = function(){
            if(! observer){
                if(! is_game_over()){
                    if(player_color == "white"){
                        user_result="bt";
                    }else{
                        user_result="wt";
                    }
                    socket.send(JSON.stringify({action : "time_over",
                                                fen : game.fen(),
                                                pgn : game.pgn(),
                                                status: statusEl.text(),
                                                result: user_result}));
                }
                updateStatus();
            }
	}
	
	
	this.socket.onmessage = function(e){
            var message = JSON.parse( e.data);
            if(message == null){
                send_update_request();
            }
            else if(message.state != undefined && ( ( message.state.player_start_time  && message.state.player_time < this_aux.player_countdown.available_time ) || ( message.state.opponent_start_time && message.state.opponent_time < this_aux.opponent_countdown.available_time ) || ( ! this_aux.init_read && ( message.state.player_start_time || message.state.opponent_start_time ) )  )){
                this_aux.init_read = true;
                this_aux.stop();
                this_aux.player_countdown=new Countdown(message.state.player_time,"player-clock",false,sdate);
                this_aux.opponent_countdown=new Countdown(message.state.opponent_time,"opponent-clock",false,sdate);
                this_aux.player_countdown.onzero = function(){
                    if(!observer){
                        if(! is_game_over()){
                            if(player_color == "white"){
                                user_result="bt";
                            }else{
                                user_result="wt";
                            }
                            socket.send(JSON.stringify({action : "time_over",
                                                        fen : game.fen(),
                                                        pgn : game.pgn(),
                                                        status: statusEl.text(),
                                                        result: user_result}));
                        }
                        updateStatus();
                    }
                }
		
                this_aux.opponent_countdown.onzero = function(){
                    send_update_request();
                }
		
                
                this_aux.player_countdown.update();
                this_aux.opponent_countdown.update();
                if(! is_game_over() ){
                    if(message.state.player_start_time){
                        this_aux.player_countdown.start(message.state.player_start_time);
                    }else if(message.state.opponent_start_time){
                        this_aux.opponent_countdown.start(message.state.opponent_start_time);
                    }
                }else{
                    if(user_result == "wt"){
                        if(player_color == "black"){
                            this_aux.player_countdown.update(0);
                        }else{
                            this_aux.opponent_countdown.update(0);
                        }
                    }else if(user_result == "bt"){
                        if(player_color == "black"){
                            this_aux.opponent_countdown.update(0);
                        }else{
                            this_aux.player_countdown.update(0);
                        }
                    }
                }
            }else{
                if(message.event == "move"){
                    if(message.from != player_color){
                        if(this_aux.player_countdown.start_time  != message.date){
                            this_aux.opponent_countdown.stop();
                            this_aux.opponent_countdown.set_available_time(message.info);
                            this_aux.player_countdown.start(message.date);
                        }
                    }else{
                        if(this_aux.opponent_countdown.start_time != message.date){
                            this_aux.player_countdown.stop();
                            this_aux.player_countdown.set_available_time(message.info);
                            this_aux.opponent_countdown.start(message.date);
                        }
                    }
                }
            }
	}
	
	
    }
 
    
    this.connect_websocket()
 

    this.stop=function(){
              this.player_countdown.stop();
              this.opponent_countdown.stop();
    }
 
    this.player_countdown.update();
    this.opponent_countdown.update();
}





var box1 = document.querySelector(".board-box");
var box2 = document.querySelector("#info-box");

var pclock = document.querySelector("#player-clock");
var oclock = document.querySelector("#opponent-clock");

var pbox =document.querySelector("#player-box");
var obox =document.querySelector("#opponent-box");
var pname =document.querySelector("#player_name");
var oname =document.querySelector("#opponent_name");
var names_div=document.querySelector("#names-box");

 

// Sets page elemets sizes
// function set_dimentions(){
//     var b=document.querySelector("#board");
//     if(board != undefined){
// 	var iinfo = document.querySelector("#inner-info");
// 	var status_box=document.querySelector(".status");
// 	if(box1.getBoundingClientRect().bottom <= box2.getBoundingClientRect().top ){
// 	    // if portrait
// 	    clocks_with_names();
// 	    board.resize()
// 	}else{
// 	    // if landascape
// 	    clocks_with_info();
// 	    board.resize()
// 	}
	
//     }
//     pgn.scrollTop=pgn.scrollHeight;
// }



// set_dimentions();
//window.addEventListener("resize", set_dimentions, false);

// function clocks_with_names(){
//     pname.parentNode.removeChild(pname);
//     oname.parentNode.removeChild(oname);
//     if(document.body.children[0] == names_div){
// 	document.body.removeChild(names_div);
//     }

//     pbox.appendChild(pname);
//     obox.appendChild(oname);
//     pbox.appendChild(pclock);
//     obox.appendChild(oclock);
//     pclock.style.fontSize="x-large";
//     oclock.style.fontSize="x-large";

// }

// function clocks_with_info(){
//     if(document.body.children[0] != names_div){
// 	document.body.insertBefore(names_div,document.body.children[0]);
//     }
//     pname.parentNode.removeChild(pname);
//     oname.parentNode.removeChild(oname);    
//     pclock.parentNode.removeChild(pclock);
//     oclock.parentNode.removeChild(oclock);
//     names_div.appendChild(oname);
//     names_div.appendChild(pname);
//     box2.insertBefore(oclock,box2.children[0]);
//     box2.appendChild(pclock);
//     oclock.style.alignSelf="flex-start";
//     pclock.style.alignSelf="flex-start";
//     pclock.style.fontSize="xx-large";
//     oclock.style.fontSize="xx-large";
    
// }


var copy_pgn=document.querySelector("#copy-png");
copy_pgn.onclick = () =>{
     navigator.clipboard.writeText(pgn.innerHTML);
};

pgn.scroll(pgn.scrollLeftMax,0);

window.onresize = () =>{
    board.resize();
}

    
