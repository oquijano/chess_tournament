"use strict";

class serverDate{
    constructor(ws_date_path,update_miliseconds=1000*60){
	// ws_date_path - websocket url for synchonizing with the server date
	// update_miliseconds - How often to synchronize in miliseconds (default = 1min)
	this.ws_date_path=ws_date_path;
	this.update_miliseconds = update_miliseconds;
	this.offset = null;
	this.precission =  Infinity ;
	this.ws_date_path=ws_date_path;
	this.close=false;
	this.reconnect_interval=null;
	this.update_interval=null;
	this.connect_websocket();
	
    }

    connect_websocket(){
	var this_aux = this;
	this.socket = new WebSocket(this.ws_date_path);

	this.socket.onopen = function(e){
	    
	    this_aux.offset_update();

	    this_aux.update_interval=setInterval(()=>{this_aux.offset_update()},
						 this_aux.update_miliseconds);

	    this_aux.reconnect_interval=setInterval(()=>{
		if( ( this_aux.socket.readyState == this_aux.socket.CLOSING || this_aux.socket.readyState == this_aux.socket.CLOSED ) && ! this_aux.close){
		    clearInterval(this_aux.reconnect_interval);
		    clearInterval(this_aux.update_interval);
		    this_aux.socket=null;
		    this_aux.connect_websocket();
		}
	    },5000);
	    
	    this_aux.socket.onmessage = function(e){
		let current_date = new Date().getTime();
		let dates = JSON.parse(e.data);
		let current_precission = current_date - dates[0];
		if(current_precission < this_aux.precission){
		    this_aux.precission = current_precission;
		    this_aux.offset = (current_date + dates[0])/2 - dates[1];}}
	    
	}
    }

    offset_update(){
	this.precission = Infinity;
	var this_aux=this;
	for (let i=0;i<10;i++){
	    setTimeout(() => {this_aux.socket.send(JSON.stringify(new Date().getTime()))},
		       i*20);
	}	
    }

    stop(){
	this.close=true;
	if(this.reconnect_interval !=null){
	    clearInterval(this.reconnect_interval);
	}
	if( this.update_interval != null){
	    clearInterval(this.update_interval);
	}	
	this.socket.close();
    }
    
    get(){
	return new Date().getTime() - this.offset;
    }
}
