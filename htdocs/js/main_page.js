var source = new EventSource("sse/1");


function start_handler(e){
    var elem = document.getElementById(e.data);
    if(elem != undefined){
	elem.innerHTML="<span class=\"started\">Playing</span>";
    }
}

function finished_handler(e){    
	location.reload();    
}


source.addEventListener("started",start_handler);
source.addEventListener("finished",finished_handler);

