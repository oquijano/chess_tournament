"use strict";

class Countdown{
    
    constructor(total_time,html_id=null,ws_date_url=false,sdate_object=false,synchronize_update=1000*3600,pretty=true){
	this.total_time = total_time;
	this.available_time = this.total_time;
	this.start_time = null;
	this.sdate=null;
	this.active_countdown = null;

	this.pretty = pretty;
	
	// Define this.update_elem
	this.set_update_elem(html_id);

	// Define current time method:
	if(sdate_object){
	    this.sdate = sdate_object;
	    this.current_time = () =>{return this.sdate.get()};
	}	
	else if(ws_date_url){
	    this.sdate = new serverDate(ws_date_url,synchronize_update);
	    this.current_time = () =>{return this.sdate.get()};
	}else{
	    this.current_time = () => {return new Date().getTime()};
	}
	
    }


    set_update_elem(html_id=null){
	if(html_id){
	    this.update_elem = document.getElementById(html_id);
	}else{
	    this.update_elem = null;
	}

    }

    format_time(seconds){
	// Taken from: https://stackoverflow.com/questions/3733227/javascript-seconds-to-minutes-and-seconds/11486026#11486026
	var hrs = ~~(seconds / 3600);
	var mins = ~~((seconds % 3600) / 60);
	var secs = ~~seconds % 60;
	var ret = "";
	if (hrs > 0) {
            ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
	}
	ret += "" + mins + ":" + (secs < 10 ? "0" : "");
	ret += "" + secs;
	return ret;
    }

    set_available_time(time){
	this.available_time = time;
    }

    set_start_time(a_start_time){
	this.start_time = a_start_time;
    }

    remaining_time(current_time=this.current_time()){
	// Returns remaining time in miliseconds
	return this.available_time - (current_time - this.start_time);
    }

    update(remaining_time=null){
	if (remaining_time == null){
	    remaining_time = this.available_time;
	}
	if(this.pretty){
	    this.update_elem.innerHTML = this.format_time( Math.floor( Math.max(remaining_time,0) /1000) );
	}else{
	    this.update_elem.innerHTML = Math.floor(  Math.floor( Math.max(remaining_time,0) /1000  )) ;
	}
    }
    
    start(start_time=new Date().getTime(),update_time=20){
	/// update_time should be in miliseconds.	
	if(this.update_elem != null){
	    this.set_start_time(start_time);
	    var this_aux = this;
	    this.active_countdown = setInterval( () => {
		var remaining_time = this_aux.remaining_time();
		if(remaining_time <= 0 ){
		    this_aux.stop();
		    if(this_aux.onzero != undefined){
			this_aux.onzero();
		    }
		}else{
		    this_aux.update(remaining_time);
		}
	    },update_time);
	}	
	
    }

    stop(){
	if(this.active_countdown != null){
	    clearInterval(this.active_countdown);
	    this.set_available_time(this.remaining_time());
	    this.active_countdown=null;
	}
    }
    
}
