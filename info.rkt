#lang info
(define collection "chess_tournament")
(define deps '("base" "sha" "rfc6455" "yaml" "threading" "csv-reading" "http-easy" "SSE"))

(define racket-launcher-libraries '("commandline_program.rkt"))
(define racket-launcher-names '("chess_tournament"))

(define build-deps '("scribble-lib" "racket-doc" "rackunit-lib"))

(define scribblings '( ("scribblings/chess_tournament.scrbl" ())
		       ("scribblings/ws-manager.scrbl" ())))

(define pkg-desc "Package for doing chess tournaments")
(define version "0.9")
(define pkg-authors '(Oscar Alberto Quijano Xacur))
