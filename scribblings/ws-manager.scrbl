#lang scribble/manual
@require[@for-label[chess_tournament/ws-manager
                    racket/base]]

@title{chess_tournament/ws-manager}

@author{Oscar Alberto Quijano Xacur}

@defmodule[chess_tournament/ws-manager]



This sub-module of defines structs and functions for managing groups of
connections that should be synchronized using websockets.

@section["Definitions"]

@defstruct[ws-messages-group ([messages-thread thread?] [messages-hash hash?])]{
A structure that manages messages between a group of related connections using
websockets. @racket[messages-thread] is a thread that receives messages from
all the connections. @racket[messages-hash] contains an element
@racket['index-last-message] which is the number of messages that have been
receive. The other elements are integers. For reach integer there is a
message. For instance, the entry with value @racket[2] corresponds to the
second message received by the group.
}

@defproc[(make-ws-messages-group [process-event (-> string? ws-messages-group? void?)])
	ws-messages-group?]{
Creates a ws-messages-group variable. It initializes it's
@racket[messages-thread] to a thread that is constantly waiting for
messages. Messages that are not strings are ignored. String messages are passed
to @racket[process-event] which decides how to handle the message.
}

@defproc[(ws-send-message [a-ws-messages-group ws-messages-group?]
			  [message any/c])
			  void?]{
Sends the contents of the string @racket[ws-messages-group] to the @racket[messages-thread] in @racket[a-ws-messages-group].
}

@defproc[(ws-missing-messages [a-ws-mg ws-messages-group?]
			      [a-index integer?])
			      (listof string?)]{
Returns a list containing all the messages in @racket[a-ws-mg] with an index
greater than @racket[a-index].
}
			      


@section["Example of use"]

The following code defines the process event function @racket[a-process-event]
and used it to define a @racket[ws-messages-group].

@codeblock|{

(define (a-process-event message a-ws-mg)    
    (define the-hash-tabe (ws-messages-group-messages-hash a-ws-mg))    
    (define current-id (hash-ref the-hash-tabe 'index-last-message ))
    (define next-id (add1 current-id))
    (hash-set! the-hash-tabe 'index-last-message next-id)
    (hash-set! the-hash-tabe next-id message))

(define ws-mg-test1 (make-ws-messages-group a-process-event))

(ws-send-message ws-mg-test1 "hello")
    
}|


;;  LocalWords:  structs websockets
