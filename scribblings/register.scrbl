#lang scribble/manual
@require[@for-label[chess_tournament/register
                    racket/base]]

@title{chess_tournament/register}

@author{Oscar Alberto Quijano Xacur}

@defmodule[chess_tournament/register]

This sub-module has functions for serving a website to which users can register
for the tournament.

It uses two parameters @racket[base-url-path] and
@racket[current-registration-db]. The first one is the name of the url that
gives a proxy to the registration website the second one is a connection to a
sqlite database for registering the players.

The players and their e-mails are stored in a table called @racket[PLAYERS] in
the columns @racket[NAME] and @racket[EMAIL].

@defproc[(start-registration-server [database-file string?]
				    [port-no positive-integer?]
				    [url-main-path string? "/"]
				    void?)]{
				    
Starts a server on port @racket[port-no] for players to register. It writes the
registered players in the sqlite database file with path
@racket[database-file]. When a proxy is used to connect to the port of this
server, @racket[url-main-path] is the path of that proxy.}
