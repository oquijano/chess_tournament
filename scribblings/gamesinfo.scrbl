#lang scribble/manual

@title{gamesinfo}

@defmodule[chess_tournament/gamesinfo]

This sub-module defines functions for storing and following the state of games.

For each game there are three codes: the @emph{white code}, the @emph{black
code} and the @emph{observer code}. These codes are added at the end of the
main url and they define the behavior of the page. When the @emph{white code}
is used, it allows to play using the white pieces. Similarly, the @emph{black
code} allows to play with the black pieces. The @emph{observer code} allows to
watch the game; it does not allow to move any piece.

Games are managed with a hash table. The keys in this hash table are codes
mentioned in the previous paragraph. Therefore, for each game there are three
entries in the hash table. The value for each key is a @racket[page-info]
struct

@defstruct[page-info ([ws-manager ws-messages-group?]
		      [role (or/c "observer" "white" "black")]
		      [additional-info (or/c list? empty)] )]{
		      
@racket[ws-manager] is the @racket[ws-messages-group] associated to the
game. @racket[role] is the role of the player, i.e. white, black or
observer. @racket[additional-info] is an association list containing
information for the particular role. For the observer it should contain
@racket['white], @racket['black] and @racket['time].  For white and black it
should at least contain the keys @racket['name], @racket['opponent_name],
@racket['opponent_code], @racket[observer_code] and @racket['time].

}

Once a hash has been created with the purpose of managing games, games can be
added using

@defproc[(add-game-info [white-name string?]
			[black-name string?]
			[games-manager-hash hash?]
			[white-code (or/c string? #f)  #f]
			[black-code (or/c string? #f)  #f]
			[observer-code (or/c string? #f) #f])
			list?]{

@racket['white-name] and @racket[black-name] are strings with the names of the
white and black players respectively. @racket[games-manager-hash] is the games
manager hash table. @bold{Special use:} If @racket[black-name] is @racket["-"],
no entry is added to the hash table. In a tournament this is used for
expressing a @hyperlink["https://en.wikipedia.org/wiki/Glossary_of_chess#bye" "bye"].

The default value of @racket[white-code], @racket[black-code] and
@racket[observer-code] is @racket[#f] which means that a random code is
generated for each of them. One can instead use string values. In this case the
string is used as the code.

It returns a list with information about the hashes generated. This list is in
the right order for inserting elements in the round tables defined in the
pairings sub-module.

}
			