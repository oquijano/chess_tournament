#lang scribble/manual

@require[@for-label[chess_tournament/pairings
		    racket/base]]

@title{chess_tournament/pairings}

@author{Oscar Alberto Quijano Xacur}

@defmodule[chess_tournament/pairings]

This module is in charge of managing the sqlite database with information about
the tournament and has functions for pairing the players. 

@defparam[current-tournament-db a-database-path connection?]{Parameter that
gets a string which should be the path to a sqlite database and becomes a
connection to that database.}

@defparam[tournament-hash a-hash-table hash?]{Parameter containing the hash
table that manages the games of the tournament.}

@defproc[(create-tournament-tables [Nrounds integer?])]{Creates the table
ROUNDS in the database and writes the number of rounds and the current
round.}

@defproc[(get-current-round integer?)]{Gets the number of the current
round. Ita takes it from the ROUNDS table in the @racket[current-tournament-db]
database}.

@defproc[(get-number-of-rounds integer?)]{Gets the total number of rounds in
the tournament. It reads the value from the ROUNDS table.}

@defproc[(create-next-round-table [a-pairing-function (-> list?)])
				  void?]{
				  
Creates the table for the next round, with the name ROUNDi where i is the
number of the next round. It also adds all the ws-managers to the games manager hash table of this round.

}

@defproc[(get-players-info) list?]{
Returns a list of vectors with two entries. The first entry is the name of a
registered player and the second one is the corresponding e-mail.}

@defproc[(random-pairing) list?]{ Returns a list of pairs, where the first name
is the name of the hite player and the second one the name of the black
player. The pair are completely randomly formed from the list of players.}

@defproc[(current-round-over?) boolean?]{
Returns @racket[#t] if all the results from the current round have been
reported in the database. Otherwise it returns #f.}