#lang racket/base


(require (for-syntax racket/base
		     syntax/parse
		     racket/syntax)
	 net/rfc6455	 
	 net/url
	 net/rfc6455/service-mapper
	 web-server/web-server
	 net/rfc6455/conn-api ;; for ws-conn-base-line
	 racket/string ;; for string-split
	 net/uri-codec ;; for form-urlencoded-decode
	 racket/string
	 json
	 racket/list
	 racket/match
	 "gamesinfo.rkt"
	 "ws-manager.rkt")

(provide games-hash
	 define-ws-service-mapper
	 ws-start-banner
	 ws-clocks
	 manage-active-connections
	 ;; for debugging
	 state-hash
	 gen-ws-filter-rx
	 )

(define (is-ws-closed? a-ws-conn)
  (or (ws-conn-closed? a-ws-conn) (port-closed? (ws-conn-base-ip a-ws-conn))   ))


(define state-hash (make-parameter (make-hash)))

;; same role than in server
(define the-ws-manager (make-parameter (void)))

;; Parameter with the information about games
;; It is compatible with the games-manager-hash used in gamesinfo.rkt
;; In tournaments its value should be (tournament-hash)
(define games-hash (make-parameter (make-hash)))

;; Parameter containing the information of a particular game for a
;; particular role. Similar to the role of game-page-info in the function
;; create-chess-ws-handler
(define game-info (make-parameter (void)))

(define page-id (make-parameter (void)))

(define role (make-parameter (void)))

(define initial-time (make-parameter (void)))

(define (opponent-code)
  (if (observer?)
      (get-black-code (game-info))
      (get-player-opponent-code (game-info))))

(define (obs-code)
  (if (observer?)
      (page-id)
      (get-observer-code (game-info)))
  )

(define (white?)
  (string=? (role) "white"))

(define (black?)
  (string=? (role) "black"))

(define (observer?)
  (string=? (role) "observer"))





(define (init-state)
  ;; This parameter saves the clock information so that it is
  ;; available after reconnecting.
  ;; Time should be the total amount of time in seconds

  ;; game-status = 0 means the game hasn't started yet
  ;; game-status = 1 means the game started
  (unless (hash-has-key? (state-hash) (page-id))
    (hash-set! (state-hash) (page-id) (make-hash `((game-status . 0)
						(available-time . ,(* 1000 (initial-time)))
						(start-time . #f)
						))))

  (unless (hash-has-key? (state-hash) (opponent-code))
    (hash-set! (state-hash) (opponent-code) (make-hash `((game-status . 0)
							 (available-time . ,(* 1000 (initial-time)))
							 (start-time . #f)
							 ))))

  
  (unless (hash-has-key? (state-hash) (obs-code))
    (hash-set! (state-hash) (obs-code) (make-hash `((game-status . 0)
							 (available-time . ,(* 1000 (initial-time)))
							 (start-time . #f)
							 ))))  
  )

(define (get-status)
  (hash-ref (hash-ref (state-hash) (page-id)) 'game-status))

(define (get-available-time)
  (hash-ref (hash-ref (state-hash) (page-id)) 'available-time))

(define (get-start-time)
  (hash-ref (hash-ref (state-hash) (page-id)) 'start-time))

(define (get-opponent-available-time)
  (hash-ref (hash-ref (state-hash) (opponent-code)) 'available-time))

(define (time-over? [threshold 2000])
  (and (get-start-time) (< (- (get-available-time) (- (current-inexact-milliseconds) (get-start-time)) )
			   (- threshold)) )
  )

(define (get-opponent-start-time)
  (hash-ref (hash-ref (state-hash) (opponent-code)) 'start-time))

(define (send-clocks-status)
  (let ([message-to-send (make-hash (list (cons 'state  (make-hash `((player_time . ,(get-available-time))
							     (opponent_time . ,(get-opponent-available-time))
							     (player_start_time . ,(get-start-time))
							     (opponent_start_time . ,(get-opponent-start-time)))))))])
   (message-self message-to-send
		 (ws-clocks)))
  )

(define (status+1)
  (hash-set! (hash-ref (state-hash) (page-id)) 'game-status 1)
  (hash-set! (hash-ref (state-hash) (opponent-code)) 'game-status 1)
  (hash-set! (hash-ref (state-hash) (obs-code)) 'game-status 1)
  )

(define (set-available-time a-time) 
  (hash-set! (hash-ref (state-hash) (page-id)) 'available-time a-time)
  (when (white?)
    (hash-set! (hash-ref (state-hash) (obs-code)) 'available-time a-time)
    ))

(define (set-opponent-available-time a-time)  
  (hash-set! (hash-ref (state-hash) (opponent-code)) 'available-time a-time)
  (when (black?)
    (hash-set! (hash-ref (state-hash) (obs-code)) 'available-time a-time)
    )
)

(define (set-start-time a-time)
  (hash-set! (hash-ref (state-hash) (page-id)) 'start-time a-time)
  (when (white?)
    (hash-set! (hash-ref (state-hash) (obs-code)) 'start-time a-time)
    )
  )

(define (set-opponent-start-time a-time)
  (hash-set! (hash-ref (state-hash) (opponent-code)) 'start-time a-time)
  (when (black?)
    (hash-set! (hash-ref (state-hash) (obs-code)) 'start-time a-time)
    ))


;;; Generates a function that every 5 minutes cleans closed
;;; connections for each code
(define (manage-active-connections ws-hash-param)
  (thread
   (lambda ()
     (let loop ()
       (sleep 300)
       (for ([code (hash-keys (ws-hash-param))])
	 (hash-set! (ws-hash-param) code
		    (filter (lambda (x) (not (is-ws-closed? x))) (hash-ref (ws-hash-param) code) ))
	 (when (empty? (hash-ref (ws-hash-param) code))
	   (hash-remove! (ws-hash-param) code )))))))


(define (add-ws-connection code a-ws-conn ws-hash)
  (if (hash-has-key? ws-hash code)
      (hash-set! ws-hash code (append (hash-ref ws-hash code) (list a-ws-conn)))
      (hash-set! ws-hash code (list a-ws-conn))))


;; Parameter hashes:
(define ws-start-banner (make-parameter (make-hash)))
;(manage-active-connections ws-start-banner)

(define ws-clocks (make-parameter (make-hash)))
;(manage-active-connections ws-clocks)

;; ws-hash should be a hash table storing whose keys are codes and
;; their values are lists of ws-connections
(define (send-message code message ws-hash)
  (when (hash-has-key? ws-hash code)
    (for ([c (hash-ref ws-hash code)])
      (unless (is-ws-closed? c)
	(with-handlers ([exn? (lambda (e) #f)])
	  (ws-send! c (jsexpr->string message)))
	)
      )))

(define (message-opponent message ws-hash)
  (send-message (opponent-code) message ws-hash))

(define (message-observer message ws-hash)
  (send-message (obs-code) message ws-hash))

(define (message-self message ws-hash)
  (send-message (page-id) message ws-hash))

(define (message-peers message ws-hash)
  (message-opponent message ws-hash)
  (message-observer message ws-hash))

(define (message-all message ws-hash)
  (message-peers message ws-hash)
  (message-self message ws-hash))





(define (get-line c)
  ;; needs: net/rfc6455/conn-api
  ;; c should be a ws-conn struct
  (ws-conn-base-line c))

(define (get-path c)
  ;; needs racket/string net/uri-codec
  ;; c should be a ws-conn struct
  (form-urlencoded-decode (cadr (string-split (bytes->string/utf-8 (get-line c))))))

(define (get-last-element-in-path a-path-string)
  ;; needs racket/string
  (last (string-split a-path-string "/")))


(define-syntax (define-ws-handler stx)
  (syntax-parse stx
		[(_ (fname:id var:id)  (before-loop-body ...) (loop-body ...))
		 #'(define (fname var)
		     before-loop-body ...
		     (let loop ()
		       (let ([message (ws-recv var)])
			 (unless (eof-object? message)
			   (match (string->jsexpr message)
			     loop-body ...
			     )
			  (loop)) 
			 ))
		     )]))

(define-ws-handler (ws-start-banner-handler a-ws-conn)
  ()
  ([(hash-table ('event message) ('date date))
    (cond
     [(string=? message "accept")
      (let ([message-to-send (make-hash `((event . ,message) (date . ,date))) ])
	(status+1)
	;;; set white start time to date + 5000
	(if (string=? (role) "white")
	    (set-start-time (+ 5000 date))
	    (set-opponent-start-time (+ 5000 date)))
	(send-message (page-id) message-to-send  (ws-start-banner))
	(message-peers message-to-send  (ws-start-banner)))
      ]
     [else (message-opponent (make-hash `((event . ,message) (date . ,date))) (ws-start-banner) )]
     )
    ])  
  )

(define-ws-handler (ws-clock-handler a-ws-conn)
  ((thread (lambda ()
   	     (let loop ()
   	       (sleep 1)
   	       (send-clocks-status)
   	       (loop)
   	       )))
   )
  ([(hash-table ('event event_type) ('date a-date) ('info info))
    (set-available-time info)
    (set-start-time #f)
    (set-opponent-start-time a-date)
    (let ([response (make-hash `((from . ,(role))
				 (event . ,event_type)
				 (date . ,a-date)
				 (info . ,info) )) ])
      
      (message-all response  (ws-clocks))      
      )
    ]
   ))

(define (gen-ws-filter-rx  endpoint [base-path "/"])
  (define corrected-games-url-path (regexp-replace #rx"/$" (regexp-replace #rx"^/+" base-path "") ""))
  (if (string=? corrected-games-url-path "")
      (string-append "^/" endpoint  "/.*" )
      (string-append "^/" corrected-games-url-path "/" endpoint  "/.*" )
      )

  )

(define (define-ws-service-mapper [games-url-path "/"])
  ;; returns two values: the regexp to include in a filter and the service mapper
  (let* ([ws-start-banner-rx-string (gen-ws-filter-rx "start_banner" games-url-path)]
	 [ws-clocks-rx-string (gen-ws-filter-rx "clocks" games-url-path )]
	 [ws-start-banner-rx (regexp ws-start-banner-rx-string)]
	 [ws-clocks-rx (regexp ws-clocks-rx-string)]
	 [ws-full-rx (regexp (format "~a|~a" ws-start-banner-rx-string ws-clocks-rx-string )) ]
	 )
    (values ws-full-rx
     (ws-service-mapper
      [ws-start-banner-rx
       [(#f) (lambda (c)
	       (page-id (get-last-element-in-path (get-path c)))
	       (when (hash-has-key? (games-hash) (page-id))
		 (game-info (hash-ref (games-hash) (page-id)))
		 (role (page-info-role (game-info)))		 		 
		 (add-ws-connection (page-id) c (ws-start-banner))
		 (message-self (make-hash `((status . ,(get-status)))) (ws-start-banner))
		 (ws-start-banner-handler c))
	       (ws-close! c)
	       )]       
       ]
      [ws-clocks-rx
       [(#f) (lambda (c)
	       (page-id (get-last-element-in-path (get-path c)))
	       (when (hash-has-key? (games-hash) (page-id))
		 (game-info (hash-ref (games-hash) (page-id)))
		 (the-ws-manager (page-info-ws-manager (game-info) ))
		 (role (page-info-role (game-info)))
		 (add-ws-connection (page-id) c (ws-clocks))
		 (initial-time (get-time (game-info)))
		 (init-state)
		 (when (> (get-status) 0)
		     (send-clocks-status))
		 (ws-clock-handler c))
	       (unless (observer?)
		 (let loop ([continue #t])
		   (sleep 2)
		   (when continue
		     (let* ([current-status-hash (ws-messages-group-messages-hash (the-ws-manager))]
			    [server-result (hash-ref current-status-hash 'result "")]
			    [game-over? (not (string=? server-result ""))])		       
		       (cond
			[game-over?
			 (loop #f)]
			[(time-over?)			 
			 (if (white?)
			     (let ([message (make-hash `((action . "time_over") (status . "Game over, White ran out of time ¡Clock!") (result . "bt") ))])
			       (ws-send-message (the-ws-manager) message )
			       (message-peers 'null (ws-clocks))
			       (loop #f))
			     (let ([message (make-hash `((action . "time_over") (status . "Game over, Black ran out of time ¡Clock!") (result . "wt") ))])
			       (ws-send-message (the-ws-manager) message )
			       (message-peers 'null  (ws-clocks))
			       (loop #f))
			     )]
			[else  (loop #t)])))) )
	       ) ]]
     
      ))))
